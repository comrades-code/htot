# Hilda: Trouble Outside Trollberg

## Notes

Working debug JSON (launches new Godot instance and goes to
specified scene)

```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "name": "GDScript Godot",
      "type": "godot",
      "request": "launch",
      "project": "${workspaceFolder}",
      "port": 6007,
      "address": "127.0.0.1",
      "launch_game_instance": true,
      "launch_scene": true,
      "scene_file": "src/levels/practice_level.tscn"
    }
  ]
}
```

* [How I got hilda themes](https://www.wikihow.com/Download-Songs-from-SoundCloud#step_2_1)
* [How I converted hilda themes from mp3 to ogg](https://godotengine.org/qa/18139/how-can-i-play-mp3-file)