# Hilda: Trouble Outside Trollberg

My first complete game, developed for fun in the [Godot](godotengine.org/) game engine. It was hacked together using assets from the cute Netflix animated series, [Hilda](https://www.netflix.com/title/80115346).

![demo](assets/img/htot_demo.gif)

`NOTES.md` contains some (disorganized) thoughts about the building process.