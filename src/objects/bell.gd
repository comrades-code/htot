extends Area2D

onready var anim_player : AnimationPlayer = \
	(self.get_node("AnimationPlayer") as AnimationPlayer)

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

func _on_Bell_body_entered(body : Node) -> void:
	if body is MyPlayer:
		self.anim_player.play("picked")
		(body as MyPlayer).pick_up_bell()