class_name Main
extends Node

var _level_0 : PackedScene = preload("res://src/levels/practice_level.tscn")
var _level_1 : PackedScene = preload("res://src/levels/level_1.tscn")
var _level_2 : PackedScene = preload("res://src/levels/level_2.tscn")
var _level_3 : PackedScene = preload("res://src/levels/level_3.tscn")
var _level_4 : PackedScene = preload("res://src/levels/level_4.tscn")

var _is_game_over : bool = false

# for some super weird reason, I have to put the these interface layer
# scene as children of a canvaslayer under main node in order
# for them to actually follow the camera when covering the screen.
onready var _pause_menu : MarginContainer = \
	(self.get_node("InterfaceLayer/PauseMenu") as MarginContainer)
onready var _quit_menu : MarginContainer = \
	(self.get_node("InterfaceLayer/QuitMenu") as MarginContainer)
onready var _hud : MyHUD = \
	( self.get_node("InterfaceLayer/HUD") as MyHUD)

# details about the current level
var _current_level : Node2D
var _current_level_num : int
var _total_sandwiches_in_current_level : int

# player node
onready var _my_player : MyPlayer = \
	(self.get_node("MyPlayer") as MyPlayer)

# music and SFX nodes
onready var _game_over_player : AudioStreamPlayer = \
	(self.get_node("GameOverPlayer") as AudioStreamPlayer)
onready var _win_player : AudioStreamPlayer = \
	(self.get_node("WinPlayer") as AudioStreamPlayer)
onready var _main_player : AudioStreamPlayer = \
	(self.get_node("MainGameThemePlayer") as AudioStreamPlayer)

func _on_hit_player() -> void:
	"""
	What happens when you are hit by a troll (lose).
	"""
	self._is_game_over = true
	self.get_tree().paused = true
	self._game_over_player.play()
	self._quit_menu.open("You lose!")

func _on_MyPlayer_arrived_at_destination() -> void:
	"""
	What happens when you reach Trolberg (you win).
	"""
	Global.update_highest_unlocked_level_num(self._current_level_num + 1)
	Global.update_levels_to_sandwiches(self._current_level_num,
		self._my_player.get_num_sandwiches(),
		self._total_sandwiches_in_current_level)
	self._is_game_over = true
	self.get_tree().paused = true
	self._win_player.play()
	self._quit_menu.open("You win!")

func init_level(num : int) -> void:
	"""
	MUST BE CALLED BEFORE THIS NODE joins the scene tree!
	A way to switch levels. Note that this currently
	relies on the entire main.tscn being re-instanced
	via the global.gd autoload/singleton. Otherwise
	we'll get weird behavior like Hilda not being in her
	start position. 
	"""
	# just like switch, no need for breaks, they're on by default
	match num:
		0:
			self._current_level = self._level_0.instance()
		1:
			self._current_level = self._level_1.instance()
		2:
			self._current_level = self._level_2.instance()
		3:
			self._current_level = self._level_3.instance()
		4:
			self._current_level = self._level_4.instance()
		# default
		_:
			# if invalid level number, just load level 0
			self._current_level = self._level_0.instance()
	self._current_level_num = num
	self._current_level.pause_mode = PAUSE_MODE_STOP
	self.add_child(self._current_level)

func _ready() -> void:
	assert(self._current_level != null)
	# this needs to be computed HERE because sandwich nodes LEAVE the scene
	# tree when picked! (as you go through the level)
	self._total_sandwiches_in_current_level = \
		self._current_level.get_node("Sandwiches").get_child_count()
	# connect signals for trolls, which are part of a level
	for troll in self._current_level.get_node("Trolls").get_children():
		# connect main script to receive player hit signals from all trolls
		if not troll.is_connected("hit_player", self, "_on_hit_player"):
			assert (troll.connect("hit_player", self, "_on_hit_player") == OK)
		# connect all trolls to receive ringing bell signal from the player
		if not self._my_player.is_connected("ring_bell", troll, "_on_MyPlayer_bell_rung"):
			assert (self._my_player.connect(
				"ring_bell", troll, "_on_MyPlayer_bell_rung") == OK)

	# connect signal from my player striking win tiles to the end game function here
	if not self._my_player.is_connected(
		"arrived_at_destination", self, "_on_MyPlayer_arrived_at_destination"):
		assert (self._my_player.connect("arrived_at_destination", self, 
			"_on_MyPlayer_arrived_at_destination") == OK)
	# connect signal from my player to update number of bells and sandwiches to
	# the appropriate callbacks in the HUD 
	if not self._my_player.is_connected("update_num_bells", 
		self._hud, "_on_MyPlayer_update_num_bells"):
		assert(self._my_player.connect("update_num_bells", 
			self._hud, "_on_MyPlayer_update_num_bells") == OK)
	if not self._my_player.is_connected("update_num_sandwiches", 
		self._hud, "_on_MyPlayer_update_num_sandwiches"):
		assert(self._my_player.connect("update_num_sandwiches", 
			self._hud, "_on_MyPlayer_update_num_sandwiches") == OK)
	self._main_player.play()
			
func _unhandled_input(event : InputEvent) -> void:
	"""
	Implements pause via 'catching' the escape input event after GUI
	processing. Note that in order of tree.paused to work, the node.paused
	property has to be appropriated set for each node in the scene tree.
	"""
	if event.is_action_pressed("ui_cancel") and not self._is_game_over:
		var tree : SceneTree = self.get_tree()
		tree.paused = not tree.paused
		if tree.paused:
			self._pause_menu.open("Game Paused")
		else:
			self._pause_menu.close()
		self.get_tree().set_input_as_handled()
