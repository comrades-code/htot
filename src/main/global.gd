extends Node

# remember: a scene is just a collection of nodes,
# which can be instanced into another scene AS a node
var current_scene : Node = null

var _current_level : int = 0
const STORE_PATH : String = "user://store.cfg"
const SAVE_DATA_SECTION : String = "save_data"
const HIGHEST_UNLOCKED_LEVEL : String = "highest_unlocked_level"
const LEVELS_TO_SANDWICHES : String = "levels_to_sandwiches"

var _highest_unlocked_level_num : int = 0
# level_num <==> Vector2(collected_sandwiches, total_sandwiches)
var _levels_to_sandwiches : Dictionary = {}

func update_highest_unlocked_level_num(level_num : int) -> void:
	if level_num > self._highest_unlocked_level_num:
		self._highest_unlocked_level_num = level_num

func update_levels_to_sandwiches(level : int,
	collected_sandwiches : int,
	total_sandwiches : int) -> void:
	# if you redo a level, don't decrease your sandwich score
	# if you did worse
	var once_collected_sandwiches : int = 0 
	if self._levels_to_sandwiches.has(level):
		once_collected_sandwiches = int(self._levels_to_sandwiches[level].x)
	self._levels_to_sandwiches[level] = Vector2(
		int(max(collected_sandwiches, once_collected_sandwiches)), total_sandwiches)

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var root : Viewport = self.get_tree().get_root()
	self.current_scene = root.get_child(
		root.get_child_count() - 1) # last child node
	self.load_game()
	# since this is an autoload (singleton), this code runs before
	# the main menu's _ready(), which is the main scene. this sets
	# the statistics appropriately before the user sees the main menu 
	if self.current_scene is MainMenu:
		self.current_scene.set_highest_unlocked_level_num(self._highest_unlocked_level_num)
		self.current_scene.set_levels_to_sandwiches(self._levels_to_sandwiches)

func goto_scene(path: String) -> void:
	# This function will usually be called from a signal callback,
	# or some other function in the current scene.
	# Deleting the current scene at this point is
	# a bad idea, because it may still be executing code.
	# This will result in a crash or unexpected behavior.

	# The solution is to defer the load to a later time, when
	# we can be sure that no code from the current scene is running:

	self.call_deferred("_deferred_goto_scene", path)
		
func _deferred_goto_scene(path : String) -> void:
	# It is now safe to remove the current scene
	self.current_scene.free()
	# Load the new scene. Same as load()?
	var s : Resource = ResourceLoader.load(path)

	# Instance the new scene.
	self.current_scene = s.instance()
	
	# there's gotta be something cleaner than this
	# for the future. why i did it:
	# https://godotengine.org/qa/4786/how-to-pass-parameters-when-instantiating-a-node
	if self.current_scene is Main:
		self.current_scene.init_level(self._current_level)
	if self.current_scene is MainMenu:
		self.current_scene.set_highest_unlocked_level_num(self._highest_unlocked_level_num)
		self.current_scene.set_levels_to_sandwiches(self._levels_to_sandwiches)

	# Add it to the active scene, as child of root.
	self.get_tree().get_root().add_child(self.current_scene)
	
	# Optionally, to make it compatible with the SceneTree.change_scene() API.
	self.get_tree().set_current_scene(self.current_scene)
	
func set_current_level(current_level : int) -> void:
	self._current_level = current_level

func _notification(what : int) -> void:
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		self.save_game()
		self.get_tree().quit() # default behavior

func load_game() -> void:
	var config_file = File.new()
	if config_file.file_exists(STORE_PATH):
		var config = ConfigFile.new()
		var rc : int = config.load(STORE_PATH)
		assert (rc == OK)
		self._highest_unlocked_level_num = config.get_value(SAVE_DATA_SECTION,
			HIGHEST_UNLOCKED_LEVEL)
		self._levels_to_sandwiches = config.get_value(SAVE_DATA_SECTION,
			LEVELS_TO_SANDWICHES)

func save_game() -> void:
	"""
	This is a really simple game, so we (ab)use the
	ConfigFile utility 
	(https://docs.godotengine.org/en/stable/classes/class_configfile.html#class-configfile)
	to store a representation of the
	data we want to save, as opposed to going down the full
	node load/save route described here:
	https://docs.godotengine.org/en/stable/tutorials/io/saving_games.html.
	"""
	var config_file = File.new()
	if not config_file.file_exists(STORE_PATH):
		config_file.open(STORE_PATH, File.WRITE)
		config_file.close()
	var config = ConfigFile.new()
	var rc : int = config.load(STORE_PATH)
	assert (rc == OK)
	config.set_value(SAVE_DATA_SECTION, HIGHEST_UNLOCKED_LEVEL, 
		self._highest_unlocked_level_num)
	config.set_value(SAVE_DATA_SECTION, LEVELS_TO_SANDWICHES,
		self._levels_to_sandwiches)
	rc = config.save(STORE_PATH)
	assert (rc == OK)
