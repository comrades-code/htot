class_name MainMenu
extends TextureRect

onready var _theme_player : AudioStreamPlayer = \
	(self.get_node("ThemeSongPlayer") as AudioStreamPlayer)
onready var _inst_menu : MarginContainer = \
	(self.get_node("InstructionMenu") as MarginContainer)

var _highest_unlocked_level_num : int = 0
# level_num <==> Vector2(collected_sandwiches, total_sandwiches)
var _levels_to_sandwiches : Dictionary = {}

func set_highest_unlocked_level_num(level_num : int):
	self._highest_unlocked_level_num = level_num

func set_levels_to_sandwiches(
	levels_to_sandwiches : Dictionary) -> void:
	self._levels_to_sandwiches = levels_to_sandwiches

func _ready() -> void:
	for level_button in self.get_node(
		"MarginContainer/VBoxContainer/VBoxContainer2/LevelButtons") \
			.get_children():
		var level_num : int = int(level_button.text)
		# hook up signal for button press
		if not level_button.is_connected(
			"pressed", 
			self, 
			"_on_LevelButton_pressed"): 
			assert (level_button.connect("pressed", 
				self, 
				"_on_LevelButton_pressed",
				[level_num]) == OK)
		# disable level buttons you haven't unlocked yet
		if level_num > 0 and level_num > self._highest_unlocked_level_num:
			level_button.disabled = true

		# apply sandwich completion counters for each level based on
		# the dictionary we have on this class
		var sandwich_count_label : Label = (level_button.get_node(
			"SandwichCtr/Count") as Label)
		if not self._levels_to_sandwiches.has(level_num):
			sandwich_count_label.text = "0/?"
		else:
			var sandwich_completion : Vector2 = \
				( self._levels_to_sandwiches.get(level_num) as Vector2)
			sandwich_count_label.text = "%d/%d" % [
				int(sandwich_completion.x), int(sandwich_completion.y)]
		
	self._theme_player.play()

func _on_LevelButton_pressed(level_num: int) -> void:
	Global.set_current_level(level_num)
	Global.goto_scene("res://src/main/main.tscn")

func _on_InstructionButton_pressed():
	self._inst_menu.visible = true
