class_name Troll
extends Area2D

signal hit_player

const ANNOYED_TIME_SECS : int = 3

var _is_annoyed : bool = false
var _is_player_colliding : bool = false

export (float) var speed : float = 100.0
var _velocity : Vector2 = Vector2(self.speed, 0.0)
onready var floor_detector_left : RayCast2D = \
	(self.get_node("FloorDetectorLeft") as RayCast2D)
onready var floor_detector_right : RayCast2D = \
	(self.get_node("FloorDetectorRight") as RayCast2D)
onready var anim_sprite : AnimatedSprite = \
	(self.get_node("AnimatedSprite") as AnimatedSprite)
onready var _annoyed_timer : Timer = \
	(self.get_node("AnnoyedTimer") as Timer)
onready var _roar_player : AudioStreamPlayer = \
	(self.get_node("RoarPlayer") as AudioStreamPlayer)

func _ready() -> void:
	"""
	Called when the node enters the scene tree for the first time.
	"""
	pass

func _physics_process(delta : float) -> void:
	"""
	Frame-rate-independent physics processing function.
	"""
	# handle the case of hilda being over a troll when the annoyance timer
	# expires
	if self._is_player_colliding and not self._is_annoyed:
		self.emit_signal("hit_player")
	var anim_to_play : String = "walk"
	if self._is_annoyed:
		anim_to_play = "annoyed"
	else:
		# remember - the floor detectors are here to cover this case
		#   __X__
		#  /     \  
		# not the opposite!
		# If the enemy encounters a wall or an edge, the horizontal velocity is flipped.
		if not self.floor_detector_left.is_colliding():
			self._velocity.x = self.speed
		elif not self.floor_detector_right.is_colliding():
			self._velocity.x = -self.speed
		self.position.x += self._velocity.x * delta

		anim_to_play = "walk"
	
	self.anim_sprite.flip_h = self._velocity.x > 0
	# because the troll's nose accounts for a good portion
	# of the sprite's width, we use an x offset on every
	# horizontal flip to better align the sprite with the
	# collision area and floor detector raycasts 
	if self.anim_sprite.flip_h:
		self.anim_sprite.offset.x = 40
	else:
		self.anim_sprite.offset.x = 0
	
	# play the appropriate sprite animation
	self.anim_sprite.play(anim_to_play)

func _on_MyPlayer_bell_rung() -> void:
	self._is_annoyed = true
	self._annoyed_timer.start(self.ANNOYED_TIME_SECS)
	self._roar_player.play()

func _on_AnnoyedTimer_timeout() -> void:
	self._is_annoyed = false

func	_on_Troll_body_entered(body : Node) -> void:
	# if troll area is entered by the ground (tilemap), reverse!
	if body is TileMap:
		self._velocity.x = -self._velocity.x
	# if the body is the player (Hilda), it's game over UNLESS
	# a timer on the troll is still ticking. this timer would be started
	# via signal when Hilda rings the bell (spacebar -> emit signal)
	if body is MyPlayer:
		if self._is_annoyed:
			self._is_player_colliding = true
		else:
			self.emit_signal("hit_player")

func _on_Troll_body_exited(body: Node) -> void:
	if body is MyPlayer:
		self._is_player_colliding = false