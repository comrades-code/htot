class_name Actor
extends KinematicBody2D
"""
Base class for actors, ex. Player.
"""

export var speed : Vector2 = Vector2(300.0, 400.0)
onready var gravity : float = ProjectSettings.get("physics/2d/default_gravity")

const FLOOR_NORMAL : Vector2 = Vector2.UP

var _velocity : Vector2 = Vector2.ZERO

func _physics_process(delta : float) -> void:
	self._velocity.y += self.gravity * delta
