class_name MyPlayer
extends Actor

const FLOOR_DETECT_DISTANCE = 20.0
signal ring_bell
signal arrived_at_destination
signal update_num_sandwiches
signal update_num_bells

var _num_bells : int = 0
var _num_sandwiches : int = 0

onready var platform_detector : RayCast2D = \
	(self.get_node("RayCast2D") as RayCast2D)
onready var anim_sprite : AnimatedSprite = \
	(self.get_node("AnimatedSprite") as AnimatedSprite)
onready var anim_player : AnimationPlayer = \
	(self.get_node("AnimationPlayer") as AnimationPlayer)

func get_num_sandwiches() -> int:
	return self._num_sandwiches

func _ready() -> void:
	"""
	Called by Godot when this node and all of its dependent nodes
	have been loaded into the SceneTree. Simply sets up a custom
	viewport for the player's (this class) camera child node.
	"""
	pass

func _physics_process(_delta : float) -> void:
	"""
	Called by Godot at an FPS-independent "physics rate" (Ex. 60 Hz)
	to apply physics (Ex. move) to the player. Note that since this
	is a KinematicBody2D, we have to apply physics (Ex. gravity) ourselves
	directly, as opposed to applying forces via _interpolate_forces().
	"""
	var direction : Vector2 = self.get_direction()

	var is_jump_interrupted : bool = Input.is_action_just_released("ui_up") \
		and self._velocity.y < 0.0
	self._velocity = self.calculate_move_velocity(
		self._velocity, 
		direction, 
		self.speed, 
		is_jump_interrupted)

	var snap_vector : Vector2 = Vector2.DOWN * self.FLOOR_DETECT_DISTANCE \
		if direction.y == 0.0 else Vector2.ZERO
	# note that platforms refer to MOVING platforms, not the regular ground
	var is_on_platform : bool = self.platform_detector.is_colliding()
	self._velocity = self.move_and_slide_with_snap(
		self._velocity, 
		snap_vector, # player remains attached to surface if this is in contact 
		self.FLOOR_NORMAL, # normal vector to floor, used for up direction
		not is_on_platform,  # stop on slope, whether or not to slide down when idle
		4, # max slides
		0.9, # floor max angle, max radians at which floor is not considered a wall
		false # infinite inertia
	)

	# a way to create a tilemap of "win tiles" (aka the door to trolberg)
	# and track collisions to it via group so that we can win the game
	# helpful: https://godotengine.org/qa/79568/can-you-connect-a-function-to-a-tilemaps-collision
	var slide_count : int = self.get_slide_count()
	if slide_count > 0:
		var collision : KinematicCollision2D = \
			self.get_slide_collision(slide_count - 1)
		var collider : Node = collision.collider
		if collider.is_in_group("win_tiles"):
			self.emit_signal("arrived_at_destination")

	# When the character’s direction changes, we want to scale
	# the AnimatedSprite accordingly to flip it.
	# This will make Hilda face left or right depending on the direction you move.
	if direction.x != 0:
		self.anim_sprite.flip_h = self._velocity.x < 0

	# select the animation to play and apply it to the AnimatedSprite
	self.anim_sprite.play(self.get_new_animation())
	# if the user hit spacebar, ring the bell (emit signal and play the animation
	# player). Note that the animated player is distinct from the animated sprite
	# for the player
	if Input.is_action_just_pressed("ui_select"):
		self.ring_bell()
		
func ring_bell() -> void:
	var anim : String = "" 
	if self._num_bells > 0:
		anim = "ringing"
		self.emit_signal("ring_bell")
		self._num_bells -= 1
		self.emit_signal("update_num_bells", self._num_bells)
	else:
		anim = "no_bell"
	if self.anim_player.current_animation != anim:
		self.anim_player.play(anim)

func pick_up_bell() -> void:
	self._num_bells += 1
	self.emit_signal("update_num_bells", self._num_bells)

func pick_up_sandwich() -> void:
	self._num_sandwiches += 1
	self.emit_signal("update_num_sandwiches", self._num_sandwiches)

func get_direction() -> Vector2:
	"""
	Returns a vector2 (where each axis has magnitude at most 1)
	corresponding to the player's current direction.
	"""
	return Vector2(
		Input.get_action_strength("ui_right") \
			- Input.get_action_strength("ui_left"),
		-1.0 if self.is_on_floor() and Input.is_action_just_pressed("ui_up") else 0.0
	)


static func calculate_move_velocity(
		linear_velocity : Vector2,
		direction : Vector2,
		speed : Vector2,
		is_jump_interrupted : bool
	) -> Vector2:
	"""
	Stateless function that calculates a new velocity whenever you need it.
  It allows you to interrupt jumps.
	"""
	var velocity : Vector2 = linear_velocity
	velocity.x = speed.x * direction.x
	if direction.y != 0.0:
		velocity.y = speed.y * direction.y
	if is_jump_interrupted:
		# Decrease the Y velocity by multiplying it, but don't set it to 0
		# as to not be too abrupt.
		velocity.y *= 0.6
	return velocity

func get_new_animation() -> String:
	"""
	Returns the name of the animation to play for this player's
	sprite based on its current velocity and whether or not it
	is in contact with the floor. 
	"""
	var animation_new : String = ""
	if self.is_on_floor():
		animation_new = "run" if abs(self._velocity.x) > 0.1 else "idle"
	else:
		animation_new = "falling" if self._velocity.y > 0 else "jumping"
	return animation_new
