class_name MyHUD
extends MarginContainer
# note how the hud.tscn that this is attached to is 
# a child of interfaceLayer (a canvaslayer), so that
# it also tracks the camera. weird!

onready var _bell_label : Label = \
	(self.get_node("HBoxContainer/BellCounter/Background/Number") as Label)
onready var _sandwich_label : Label = \
	(self.get_node("HBoxContainer/SandwichCounter/Background/Number") as Label)

func _ready() -> void:
	# @TODO - reach directly into player node to get
	# INITIAL num_sandwiches and num_bells
	pass 

# called every frame rate (where delta is the elapsed number
# of frames since the last invocation?)
# func _process(_delta) -> void:
# 	# round the animated health (not the real health!) before
# 	# applying it to the UI
# 	var rounded_health : int = round(self.animated_health)
# 	self.number_label.text = str(rounded_health)

func _on_MyPlayer_update_num_bells(num_bells : int) -> void:
	self._bell_label.text = str(num_bells)

func _on_MyPlayer_update_num_sandwiches(num_sandwiches : int) -> void:
	self._sandwich_label.text = str(num_sandwiches)