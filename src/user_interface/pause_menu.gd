extends MarginContainer

onready var resume_button : Button = \
	(self.get_node("VBoxContainer/ResumeButton") as Button)
onready var _label : Label = \
	( self.get_node("VBoxContainer/Label") as Label)

func _ready() -> void:
	self.visible = false

func close() -> void:
	self.visible = false

func open(label_text: String) -> void:
	self._label.text = label_text
	self.visible = true
	self.resume_button.grab_focus()

func _on_ResumeButton_pressed() -> void:
	self.get_tree().paused = false
	self.visible = false

func _on_QuitButton_pressed() -> void:
	# this locks up other things, so we need to unpause here
	# too
	self.get_tree().paused = false
	Global.goto_scene("res://src/main/main_menu.tscn")
